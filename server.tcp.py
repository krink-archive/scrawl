#!/usr/bin/env python

import asyncore
from scrawl import scrawl

if __name__ == "__main__":

    server = scrawl.ScrawlTCPServer('0.0.0.0', 1201, '/scrawl')
    asyncore.loop()


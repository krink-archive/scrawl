#!/usr/bin/env python

import sys
sys.dont_write_bytecode = True

#print str(sys.version_info[0])
#print str(sys.version_info[1])
if sys.version_info[1] < 7:
    WARN =  'Warning: 100% cpu usage when using asyncore with UNIX socket '
    WARN += 'use python >= 2.7 https://bugs.python.org/issue12502 '
    print str(WARN)

import asyncore
import os
import stat
from scrawl import scrawl

if __name__ == "__main__":

    if sys.argv[1:]:
        socket = sys.argv[1]
    else:
        socket = "/app/scrawl/scrawld.sock"

    if os.path.exists(socket):
        os.remove(socket)

    sys.path.insert(0, '/usr/libexec/scrawl')
    import config
    scribe_host = config.scribe['host']
    scribe_port = config.scribe['port']
    scrawl_host = config.scrawl['host']
    scrawl_port = config.scrawl['port']
    send_proto = config.send['proto']

    if send_proto == 'scribe':
        server = scrawl.ScrawlSocketServerToScribe(socket, scribe_host, scribe_port)
    elif send_proto == 'scrawl':
        server = scrawl.ScrawlSocketServerToScrawl(socket, scrawl_host, scrawl_port)
    else:
        print 'unknown send_proto' + str(send_proto)
        sys.exit(1)
    try:
        os.chmod(socket, 0777)
        asyncore.loop()
    finally:
        if os.path.exists(socket):
            os.unlink(socket)



#!/usr/bin/env python

from scrawl import scrawl

if __name__ == "__main__":

    category = 'my_test v2'
    message  = 'my_test message v2'

    import sys
    if sys.argv[1:]:
        host = sys.argv[1]
    else:
        host = 'localhost'

    port = 1463

    client = scrawl.ScribeClient(host, port)
    result = client.send(category, message)
    if result == 0:
        print 'success'



#!/usr/bin/env php
<?php

$category = 'php_testing scrawl v2.4';
$message  = 'This is a test of the general php 2.4 client!';

define('socket', '/var/lib/scrawl/scrawl.sock');

require_once 'scrawl.php';

$scrawl = new SCRAWL();
$result = $scrawl->send(socket, $category, $message);
//$result = $scrawl->sendtcp(host, port, $category, $message);
if ($result != '6') { //#ACK chr(6) (acknowledge)
    echo 'fail ' . $result . PHP_EOL;
} else {
    echo 'success ' . PHP_EOL;
}

?>


%define bindir  /usr/bin
%define sbindir /usr/sbin

Summary: Scrawl Server Daemon and Client
Name: scrawld
Version: v2.5.0.0.0
Release: 2%{?dist}
License: GPL
URL: http://mirror/servers/setup/scrawl.git
Group: Applications/Internet
Source0: scrawl-%{version}.tgz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

#Requires: python, scribe-python, python-thrift
# rh6 no pkg support for scribe-python, python-thrift,
# configure scrawl only...

%if 0%{?rhel} == 7
Requires: python
Requires: scribe-python, python-thrift
%endif

%if 0%{?rhel} == 6
# 100% cpu usage when using asyncore with UNIX socket
# https://bugs.python.org/issue12502
AutoReqProv: no
Requires: python27
Requires: scribe-python, thrift-python
%endif

Requires(pre): /usr/sbin/useradd, /usr/bin/getent
Requires(postun): /usr/sbin/userdel

Provides: scrawld

%description
Server scrawld daemon

%prep
tar xzvf %{SOURCE0}

#%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{bindir}
 
cp scrawl-%{version}/server.py $RPM_BUILD_ROOT/%{bindir}/scrawld
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawld

cp scrawl-%{version}/client.py $RPM_BUILD_ROOT/%{bindir}/scrawl.client.socket
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl.client.socket

cp scrawl-%{version}/list.backlog.py $RPM_BUILD_ROOT/%{bindir}/scrawl-backlog
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl-backlog

cp scrawl-%{version}/purge.backlog.py $RPM_BUILD_ROOT/%{bindir}/scrawl-purge-backlog
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl-purge-backlog

mkdir -p $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/scrawl.py

cp scrawl-%{version}/scribe_log $RPM_BUILD_ROOT/usr/libexec/scrawl/scribe_log
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scribe_log

#cp scrawl-%{version}/scrawl_log $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl_log
#chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl_log

#cp scrawl-%{version}/scrawl_log $RPM_BUILD_ROOT/%{bindir}/scrawl_log
#chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl_log

#mkdir -p $RPM_BUILD_ROOT/etc
#cp scrawl-%{version}/scrawl.conf $RPM_BUILD_ROOT/etc/

%if 0%{?rhel} == 7
mkdir -p $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl/scrawl.py

mkdir -p $RPM_BUILD_ROOT/lib/systemd/system
cp scrawl-%{version}/scrawld.service $RPM_BUILD_ROOT/lib/systemd/system/scrawld.service
%endif

%if 0%{?rhel} == 6

#mkdir -p $RPM_BUILD_ROOT/etc/profile.d
#cp scrawl-%{version}/rh6/rh6.python27.env.txt $RPM_BUILD_ROOT/etc/profile.d/python27.sh

#/opt/rh/python27/root/usr/lib64/python2.7/site-packages
#/usr/lib/python2.6/site-packages

mkdir -p $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl/scrawl.py

mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.py

touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.pyc
touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.pyo

touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.pyc
touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.pyo

#mkdir -p $RPM_BUILD_ROOT/usr/libexec/scrawl

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/client.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/client.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/client.tcp.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/client.tcp.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/purge.backlog.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/purge.backlog.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/server.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/server.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/server.tcp.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/server.tcp.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/client.scribe.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/client.scribe.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/config.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/config.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/scrawl.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/scrawl.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/__init__.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/__init__.pyo

mkdir -p $RPM_BUILD_ROOT/etc/init.d
cp scrawl-%{version}/scrawld.init $RPM_BUILD_ROOT/etc/init.d/scrawld
chmod 755 $RPM_BUILD_ROOT/etc/init.d/scrawld

# carry over python27 scribe
mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe
cp scrawl-%{version}/rh6/lib/python2.7/site-packages/scribe-2.0-py2.7.egg-info $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe-2.0-py2.7.egg-info
cp scrawl-%{version}/rh6/lib/python2.7/site-packages/scribe/*.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe/

mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303
cp scrawl-%{version}/rh6/lib/python2.7/site-packages/fb303-1.0-py2.7.egg-info $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303-1.0-py2.7.egg-info
cp scrawl-%{version}/rh6/lib/python2.7/site-packages/fb303/*.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303/

# carry over python27 thrift
mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift
mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol
mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/server
mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport
cp scrawl-%{version}/rh6/lib64/python2.7/site-packages/thrift-0.9.1-py2.7.egg-info $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift-0.9.1-py2.7.egg-info
cp scrawl-%{version}/rh6/lib64/python2.7/site-packages/thrift/*.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/
cp scrawl-%{version}/rh6/lib64/python2.7/site-packages/thrift/protocol/*.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol/
cp scrawl-%{version}/rh6/lib64/python2.7/site-packages/thrift/server/*.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/server/
cp scrawl-%{version}/rh6/lib64/python2.7/site-packages/thrift/transport/*.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/

%endif


mkdir -p $RPM_BUILD_ROOT/usr/libexec/scrawl
mkdir -p $RPM_BUILD_ROOT/var/spool/scrawl
mkdir -p $RPM_BUILD_ROOT/var/lib/scrawl
mkdir -p $RPM_BUILD_ROOT/var/log/scrawl

cp scrawl-%{version}/client.scribe.py $RPM_BUILD_ROOT/usr/libexec/scrawl/client.scribe.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/client.scribe.py

cp scrawl-%{version}/server.py $RPM_BUILD_ROOT/usr/libexec/scrawl/server.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/server.py

cp scrawl-%{version}/server.tcp.py $RPM_BUILD_ROOT/usr/libexec/scrawl/server.tcp.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/server.tcp.py

cp scrawl-%{version}/client.py $RPM_BUILD_ROOT/usr/libexec/scrawl/client.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/client.py

cp scrawl-%{version}/client.tcp.py $RPM_BUILD_ROOT/usr/libexec/scrawl/client.tcp.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/client.tcp.py

cp scrawl-%{version}/config.py $RPM_BUILD_ROOT/usr/libexec/scrawl/config.py
chmod 644 $RPM_BUILD_ROOT/usr/libexec/scrawl/config.py

cp scrawl-%{version}/client.php $RPM_BUILD_ROOT/usr/libexec/scrawl/client.php
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/client.php

cp scrawl-%{version}/scrawl.php $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl.php

cp scrawl-%{version}/scribe_log $RPM_BUILD_ROOT/usr/libexec/scrawl/scribe_log
#cp scrawl-%{version}/scrawl_log $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl_log


%clean
rm -rf $RPM_BUILD_ROOT

%pre
# Add user/group here if needed...
echo "Add user/group here if needed..." >/dev/null 2>&1
/usr/bin/getent group scrawl > /dev/null || /usr/sbin/groupadd -r scrawl
/usr/bin/getent passwd scrawl > /dev/null || /usr/sbin/useradd -r -d /usr/libexec/scrawl -s /sbin/nologin -g scrawl scrawl

%post
# Add serivces for startup
%if 0%{?rhel} == 6
  echo "rh6"
  echo "install /etc/init.d/scrawld"
       /sbin/chkconfig --add scrawld 
  if [ $1 = 1 ]; then #1 install
    echo "start scrawld"
        /etc/init.d/scrawld start
  else
    echo "restart scrawld"
        /etc/init.d/scrawld restart
  fi
%endif

%if 0%{?rhel} == 7
  echo "systemctl daemon-reload"
        systemctl daemon-reload
  if [ $1 = 1 ]; then #1 install
    echo "systemctl enable scrawld"
          systemctl enable scrawld
    mkdir -p /var/spool/scrawl/backlog >/dev/null 2>&1
    chown scrawl /var/spool/scrawl/backlog >/dev/null 2>&1
    echo "systemctl start scrawld"
          systemctl start scrawld
  else
    echo "systemctl restart scrawld"
          systemctl restart scrawld
  fi
%endif

ln -s /usr/libexec/scrawl/config.py /etc/scrawl.conf
#end post

%postun
rm -f /etc/scrawl.conf

%files
%defattr(-,root,root)
%{bindir}/scrawld
%{bindir}/scrawl.client.socket
#%{bindir}/scrawl_log
%{bindir}/scrawl-backlog
%{bindir}/scrawl-purge-backlog

%if 0%{?rhel} == 7
/usr/lib/python2.7/site-packages/scrawl/__init__.py
/usr/lib/python2.7/site-packages/scrawl/scrawl.py
/lib/systemd/system/scrawld.service
%endif

%if 0%{?rhel} == 6
/usr/lib/python2.6/site-packages/scrawl/__init__.py
/usr/lib/python2.6/site-packages/scrawl/scrawl.py
/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.py
/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.py
/etc/init.d/scrawld

   /opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303-1.0-py2.7.egg-info
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303/FacebookBase.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303/FacebookService.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303/__init__.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303/constants.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/fb303/ttypes.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe-2.0-py2.7.egg-info
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe/__init__.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe/constants.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe/scribe.py
   /opt/rh/python27/root/usr/lib/python2.7/site-packages/scribe/ttypes.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift-0.9.1-py2.7.egg-info
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/TSCons.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/TSerialization.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/TTornado.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/Thrift.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/__init__.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol/TBase.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol/TBinaryProtocol.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol/TCompactProtocol.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol/TJSONProtocol.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol/TProtocol.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/protocol/__init__.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/server/THttpServer.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/server/TNonblockingServer.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/server/TProcessPoolServer.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/server/TServer.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/server/__init__.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/THttpClient.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/TSSLSocket.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/TSocket.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/TTransport.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/TTwisted.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/TZlibTransport.py
   /opt/rh/python27/root/usr/lib64/python2.7/site-packages/thrift/transport/__init__.py

#/etc/profile.d/python27.sh
%endif

%dir /usr/libexec/scrawl
%dir /usr/libexec/scrawl/scrawl
%dir %attr(0755, scrawl, scrawl) /var/lib/scrawl
%dir %attr(0755, scrawl, scrawl) /var/spool/scrawl
%dir %attr(0755, scrawl, scrawl) /var/log/scrawl
%config(noreplace) /usr/libexec/scrawl/config.py
/usr/libexec/scrawl/scrawl/__init__.py
/usr/libexec/scrawl/scrawl/scrawl.py
/usr/libexec/scrawl/client.py
/usr/libexec/scrawl/client.tcp.py
/usr/libexec/scrawl/client.scribe.py
/usr/libexec/scrawl/client.php
/usr/libexec/scrawl/scrawl.php
/usr/libexec/scrawl/server.py
/usr/libexec/scrawl/server.tcp.py
/usr/libexec/scrawl/scribe_log
#/usr/libexec/scrawl/scrawl_log
#%attr(0755, scrawl, scrawl) /usr/libexec/scrawl/purge.backlog.py

%if 0%{?rhel} == 7
%exclude /usr/lib/python2.7/site-packages/scrawl/*.pyc
%exclude /usr/lib/python2.7/site-packages/scrawl/*.pyo
%endif

%if 0%{?rhel} == 6
%exclude /usr/lib/python2.6/site-packages/scrawl/*.pyc
%exclude /usr/lib/python2.6/site-packages/scrawl/*.pyo
%exclude /opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/*.pyc
%exclude /opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/*.pyo
%endif

%exclude /usr/libexec/scrawl/scrawl/*.pyc
%exclude /usr/libexec/scrawl/scrawl/*.pyo
%exclude /usr/libexec/scrawl/*.pyc
%exclude /usr/libexec/scrawl/*.pyo

%changelog
* Wed May 16 2018 Karl Rink <karl@usaepay.com> v2.5.0.0.0-2
- scrawld, rh6 updates for startup init scripts

* Wed May 16 2018 Karl Rink <karl@usaepay.com> v2.5.0.0.0-1
- separate/split scrawld (scribe libs) from scrawl_log

* Sat Apr 21 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.8-2
- scrawl_log

* Thu Jan 25 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.8
- _00000

* Wed Jan 24 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.7-2
- rh6 re-pkg, carry scribe and thrift python27 site-packages

* Tue Jan 23 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.7-1
- rh6 compat/install

* Sun Jan 21 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.6-1
- v2.4 header/protocol

* Mon Jan 08 2018 Karl Rink <karl@usaepay.com> v2.3.0.0.9-1
- initial rpm


#!/usr/bin/env python

from scrawl import scrawl

import os, errno

if __name__ == "__main__":

    #scrawl_file = '/var/spool/scrawl/scrawl_spool_file00000'
    #scrawl_spool_dir_file = '/var/spool/scrawl/.backlog/scrawl_v2.3/scrawl_v2.3_00000'

    #scribe_host = 'localhost'
    #scribe_port = 1463

    import sys
    sys.dont_write_bytecode = True
    sys.path.insert(0, '/usr/libexec/scrawl')
    import config
    scribe_host = config.scribe['host']
    scribe_port = config.scribe['port']
    scrawl_host = config.scrawl['host']
    scrawl_port = config.scrawl['port']
    send_proto = config.send['proto']

    #scrawl_backlog_dir = '/var/spool/scrawl/.backlog'
    scrawl_backlog_dir = '/var/spool/scrawl/backlog'

    dirList = os.listdir(scrawl_backlog_dir)

    for category in dirList:
        #print str(category)
        try:
            os.rmdir(scrawl_backlog_dir + '/' + category)
        except OSError as e:
            if e.errno == errno.ENOTEMPTY:
                print "directory not empty"

        scrawl_spool_dir_file = scrawl_backlog_dir + '/' + category + '/' + category + '_00000'
        if os.path.isfile(scrawl_spool_dir_file):
            if os.stat(scrawl_spool_dir_file).st_size == 0:
                os.unlink(scrawl_spool_dir_file)
            else:
                print str('scrawl_spool_dir_file ' + scrawl_spool_dir_file)
                if send_proto == 'scribe':
                    print 'proto send scribe ' + str(scribe_host) + ' port ' + str(scribe_port)
                    drain = scrawl.ScrawlSpoolDirFileToScribeRetryMemTimeOutSaveBack(scrawl_spool_dir_file, scribe_host, scribe_port)
                    send = drain.send()
                elif send_proto == 'scrawl':
                    print 'proto send scrawl ' + str(scrawl_host) + ' port ' + str(scrawl_port)
                    drain = scrawl.ScrawlSpoolDirFileToScrawlTCPRetryMemTimeOutSaveBack(scrawl_spool_dir_file, scrawl_host, scrawl_port)
                    send = drain.send()
                else:
                    print 'Unknown send_proto ' + str(send_proto)

                print str(send)



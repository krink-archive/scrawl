#!/usr/bin/env python

from scrawl import scrawl

if __name__ == "__main__":

    category = 'scrawl_v2.4'
    message  = 'yep, this is a message v2.4'

    client = scrawl.ScrawlSocketClient("/var/lib/scrawl/scrawl.sock")
    result = client.send(category, message)
    print str(result)



#!/usr/bin/env python

import sys
sys.dont_write_bytecode = True

from scrawl import scrawl

if __name__ == "__main__":

    category = 'ScrawlTCPClient v2.5'
    message  = 'my ScrawlTCPClient message v2.5'

    client = scrawl.ScrawlTCPClient('localhost', 1201)
    result = client.send(category, message)

    if result is True:
        print str(result)
        sys.exit(0)
    else:
        print str(result)
        sys.exit(1)


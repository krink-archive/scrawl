
# 100% cpu usage when using asyncore with UNIX socket
https://bugs.python.org/issue12502

centos 6.9 has python 2.6 as default

/usr/bin/python2.6 asyncore_test.py
  PID USER      PR  NI  VIRT  RES  SHR S %CPU %MEM    TIME+  COMMAND
14590 root      20   0  151m 5748 2984 R 99.9  0.3   1:42.79 python2.6

sclo repo has python2.7
/opt/rh/python27/root/usr/bin/python2.7 asyncore_test.py

# ps -o pcpu,pid,ppid,cmd -p 14595
%CPU   PID  PPID CMD
 0.0 14595 14563 /opt/rh/python27/root/usr/bin/python2.7 asyncore_test.py



#!/usr/bin/env python

from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol
from thrift.server import TServer

from scrawl.TScrawl import TScrawl
#from pygen.example import Example
#from gen-py.example import Example


class ExampleHandler:
    def __init__(self):
        self.log = {}

    def ping(self):
        return "pong"

    def say(self, msg):
        print(msg)


handler = ExampleHandler()
processor = TScrawl.Processor(handler)
transport = TSocket.TServerSocket(port=30303)
tfactory = TTransport.TBufferedTransportFactory()
pfactory = TBinaryProtocol.TBinaryProtocolFactory()

server = TServer.TSimpleServer(processor, transport, tfactory, pfactory)

print("Starting python server...")
server.serve()
print("done!")


#!/usr/bin/env python

from thrift import Thrift
from thrift.transport import TSocket
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol

#from pygen.example import Example
from scrawl.TScrawl import TScrawl


try:
    transport = TSocket.TSocket('localhost', 30303)
    transport = TTransport.TBufferedTransport(transport)
    protocol = TBinaryProtocol.TBinaryProtocol(transport)
    client = TScrawl.Client(protocol)
    transport.open()
    print(client.ping())
    client.say('Hello from Python!')
    transport.close()

except Thrift.TException as tx:
    print(tx.message)




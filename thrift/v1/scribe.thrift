# https://github.com/facebookarchive/scribe/blob/master/if/scribe.thrift

include "fb303/if/fb303.thrift"

namespace cpp scribe.thrift
namespace java scribe.thrift
namespace perl Scribe.Thrift

enum ResultCode
{
  OK,
  TRY_LATER
}

struct LogEntry
{
  1:  string category,
  2:  string message
}

service scribe extends fb303.FacebookService
{
  ResultCode Log(1: list<LogEntry> messages);
}



%define bindir  /usr/bin
%define sbindir /usr/sbin

Summary: Scrawl TCP Server
Name: scrawl_tcp
Version: v2.5.0.0.0
Release: 1%{?dist}
License: GPL
URL: http://mirror/servers/setup/scrawl.git
Group: Applications/Internet
Source0: scrawl-%{version}.tgz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%if 0%{?rhel} == 7
Requires: python
%endif

%if 0%{?rhel} == 6
Requires: python
%endif

Requires(pre): /usr/sbin/useradd, /usr/bin/getent
Requires(postun): /usr/sbin/userdel

Provides: scrawl_tcp

%description
Server scrawl_tcp is a TCP server

%prep
tar xzvf %{SOURCE0}

#%build

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{bindir}
 
cp scrawl-%{version}/server.tcp.py $RPM_BUILD_ROOT/%{bindir}/scrawl_tcp
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl_tcp

mkdir -p $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/scrawl.py

cp scrawl-%{version}/client.tcp.py $RPM_BUILD_ROOT/usr/libexec/scrawl/client.tcp.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/client.tcp.py

cp scrawl-%{version}/client.py $RPM_BUILD_ROOT/usr/libexec/scrawl/client.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/client.py

cp scrawl-%{version}/server.tcp.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl_tcp
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl_tcp


%if 0%{?rhel} == 7
mkdir -p $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl/scrawl.py

mkdir -p $RPM_BUILD_ROOT/lib/systemd/system
cp scrawl-%{version}/scrawl_tcp.service $RPM_BUILD_ROOT/lib/systemd/system/scrawl_tcp.service
%endif

%if 0%{?rhel} == 6

mkdir -p $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl/scrawl.py

mkdir -p $RPM_BUILD_ROOT/etc/init.d
cp scrawl-%{version}/scrawl_tcp.init $RPM_BUILD_ROOT/etc/init.d/scrawl_tcp
chmod 755 $RPM_BUILD_ROOT/etc/init.d/scrawl_tcp

%endif


%clean
rm -rf $RPM_BUILD_ROOT

%pre
# Add user/group here if needed...
echo "Add user/group here if needed..." >/dev/null 2>&1
/usr/bin/getent group scrawl > /dev/null || /usr/sbin/groupadd -r scrawl
/usr/bin/getent passwd scrawl > /dev/null || /usr/sbin/useradd -r -d /usr/libexec/scrawl -s /sbin/nologin -g scrawl scrawl

%post
# Add serivces for startup
%if 0%{?rhel} == 6
  echo "rh6"
  echo "install /etc/init.d/scrawl_tcp"
       /sbin/chkconfig --add scrawl_tcp 
  if [ $1 = 1 ]; then #1 install
    echo "start scrawl_log"
        /etc/init.d/scrawl_tcp start
  else
    echo "restart scrawl_tcp"
        /etc/init.d/scrawl_tcp restart
  fi
%endif

%if 0%{?rhel} == 7
  echo "systemctl daemon-reload"
        systemctl daemon-reload
  if [ $1 = 1 ]; then #1 install
    echo "systemctl enable scrawl_tcp"
          systemctl enable scrawl_tcp
    echo "systemctl start scrawl_tcp"
          systemctl start scrawl_tcp
  else
    echo "systemctl restart scrawl_tcp"
          systemctl restart scrawl_tcp
  fi
%endif

%postun
echo "scrawl_tcp postun" >/dev/null

%files
%defattr(-,root,root)
%{bindir}/scrawl_tcp

%if 0%{?rhel} == 7
/usr/lib/python2.7/site-packages/scrawl/__init__.py
/usr/lib/python2.7/site-packages/scrawl/scrawl.py
/lib/systemd/system/scrawl_tcp.service
%endif

%if 0%{?rhel} == 6
/usr/lib/python2.6/site-packages/scrawl/__init__.py
/usr/lib/python2.6/site-packages/scrawl/scrawl.py
/etc/init.d/scrawl_tcp
%endif

%if 0%{?rhel} == 7
%exclude /usr/lib/python2.7/site-packages/scrawl/*.pyc
%exclude /usr/lib/python2.7/site-packages/scrawl/*.pyo
%endif

%if 0%{?rhel} == 6
%exclude /usr/lib/python2.6/site-packages/scrawl/*.pyc
%exclude /usr/lib/python2.6/site-packages/scrawl/*.pyo
%endif

/usr/libexec/scrawl/scrawl/__init__.py
/usr/libexec/scrawl/scrawl/scrawl.py
/usr/libexec/scrawl/client.py
/usr/libexec/scrawl/client.tcp.py
/usr/libexec/scrawl/scrawl_tcp

%exclude /usr/libexec/scrawl/scrawl/*.pyc
%exclude /usr/libexec/scrawl/scrawl/*.pyo
%exclude /usr/libexec/scrawl/*.pyc
%exclude /usr/libexec/scrawl/*.pyo


%changelog
* Wed May 16 2018 Karl Rink <karl@usaepay.com> v2.5.0.0.0-1
- scrawl_tcp server


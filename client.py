#!/usr/bin/env python

import sys
sys.dont_write_bytecode = True

from scrawl import scrawl

if __name__ == "__main__":

    if sys.argv[1:]:
        category = sys.argv[1]
        message  = sys.argv[2]
    else:
        category = 'TESTING scrawl_v2.5'
        message  = 'yep, this is a message v2.5'

    client = scrawl.ScrawlSocketClient("/app/scrawl/scrawld.sock")
    result = client.send(category, message)

    if result is True:
        print str(result)
        sys.exit(0)
    else:
        print str(result)
        sys.exit(1)


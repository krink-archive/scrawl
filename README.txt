
this.proto = scrawl

```
python
    client = scrawl.ScrawlSocketClient("/var/lib/scrawl/scrawl.sock")
    result = client.send(category, message)

/usr/libexec/scrawl/client.py
True
```
```
php
    $scrawl = new SCRAWL();
    $socket = "/var/lib/scrawl/scrawl.sock";
    $result = $scrawl->send($socket, $category, $message);

/usr/libexec/scrawl/client.php
success
```
                        
in-sink                   out-sink -> success = delivered
    |- sink:mem -> out-drain -|    -> failure = spool:file
                             
in-sink:scrawl:local:unix:socket
sink:spool:mem|file
out-drain:scrawl:remote:tcp
out-drain:scribe:remote:tcp

#native scrawl  
socket = '/var/lib/scrawl/scrawl.sock'
port   = 9099
spool  = '/var/spool/scrawl/'

#scribe
port = 1463

Scrawl default dir/file storage logic
/dir/file_00000
/name/name_00000
/category/category_00000

Scrawl v2.4
// first byte is the flag
// second byte is the category length (max 255)
// third and forth are the message length (max 256*256 = 65,536)

first 4 bytes are fixed length
|------header-------|----category-------|------message----------------|
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
| 1  |  2 |  3    4 |                                        
+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
first 1 byte is flag
second 1 byte is catetory size via chr/ord
third and fourth 1 byte are message size via chr/ord concatenate

1.flag 2.chr(catlen) 3.chr(floor(msglen/256)) 4.chr(msglen%256)

#########################################################################
#Scrawl client msg v2.3
#flag = 'A'
#catsz = len(category).zfill(3)
#header = str(flag) + str(catsz)
#msg = header + category + message
#```
#$flag = 'A';
#$catln = strlen($category);
#$catsz = str_pad($catln, 3, 0, STR_PAD_LEFT);
#$header = $flag . $catsz;
#$msg = $header . $category . $message;
#```
#
#first 4 bytes are fixed len.  remaining are varialbe len
#|-------------------|-------------------------------------------------|
#+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
#| 1  |      2       |       3           |              4              |
#+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
#first 1 byte is flag, next three bytes are len(cat) as lft padding zero
#
#1.flag 2.len(cat) 3.cat 4.msg
#
#example 4 byte header would be A018
#
#Limitations:
# - a category name has max length of 999 digits
#########################################################################

default  - out-drain:scrawl:remote:tcp
optional - out-drain:scribe:remote:tcp

The Scribe Client
 - relies on 
   # yum install scribe-python
   # yum install python-thrift
```
    client = scrawl.ScribeClient(host, port)
    result = client.send(category, message)
    if result == 0:
        print 'success'
```

Build Client RPM
-------------------------
To build package directly from git repo:
```
git archive --format=tar --remote=git@gitlab.usaepay.dev:console/scrawl.git \
  --prefix=scrawl-v2.3.0.0.8/ master | gzip > scrawl-v2.3.0.0.8.tgz
rpmbuild -tb scrawl-v2.3.0.0.8.tgz
```

Asci Table
---
http://www.asciitable.com/

Protocol
----

| Client | | Server |
| --- | --- | --- |
| message | -> |  |
|         | <- | ACK (chr(6)) |
| message | -> |  |
|         | <- | ACK (chr(6)) |
| EOT chr(4) | ->  |   |

If server doesn't get a full message within 5 seconds,  send a nak

| Client | | Server |
| --- | --- | --- |
| Corrupt message | -> | |
|     | <- | NAK chr(21) |
| Retransmit | -> | |
|            | <- | ACK |




PHP Example For building message
---

```
<?php


define('flag_log',1);
define('flag_gzip',2);
define('flag_bzip',4);
define('flag_jumbo',8);

$category='Hello';
$mesg = 'This is cool';


// all messages will have flag_log
$flag = flag_log;

// limit category to 255 length
$category = substr($category, 0, 255);

// calc category length
$catlen = strlen($category);

// calc message length
$len = strlen($mesg);

// if message is longer than 65,536,  we need to set the jumbo flag 
//  (to tell the server to look for a longer message)
if($len>65,536) $flag+=flag_jumbo; 

$out = 
	chr($flag) .    // first byte is the flag
	chr($catlen);   // second byte is the category length (max 255)
	
// if the jumbo flag is set,  the next 5 bytes are the message length
if($flag & flag_jumbo) {  

	$tmp = array();
	for($i=0; $i<5; $i++)
	{
		$tmp .= chr($len%256);
		$len = floor($len/256);
	}
	
	$out.=strrev($tmp);

// third and forth are the message length (max 256*256 = 65,536)
} else {
	$out .= chr(floor($len/256)) . chr($len%256);
}

// add the category
$out.=$category;

// add the message
$out.=$mesg;

```

redhat 6 support...
no packages for scribe-python, python-thrift

[vagrant@build scrawl]$ rpm -qa | grep scribe
scribe-2.2.2-ue3.el7.centos.x86_64



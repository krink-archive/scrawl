
# scrawl
__version__ = 'v2.5.0.0.0-1-1'

import asyncore

import sys
sys.dont_write_bytecode = True

import socket, errno, os, stat

import math

import logging
#DEBUG,INFO,WARNING,CRITICAL
logging.basicConfig(level=logging.INFO, format='%(levelname)s %(message)s')
log = logging.getLogger(__name__)

SPOOL = '/var/spool/scrawl'
EVENTLOGS = '/scrawl'
SIZE = 1024

drain = True

import threading
import time

import multiprocessing

debug = False

#path  = '/var/lib/scrawl/scrawl.sock'

#NUL chr(0)
#SOH chr(1)  (start of heading)
#STX chr(2) (start of text)
#ETX chr(3) (end of text)
#EOT chr(4) (end of transmission)
#ENQ chr(5) (enquiry)
#ACK chr(6) (acknowledge)

#NAK chr(21) (negative acknowledge)

class ScrawlTCPServer(asyncore.dispatcher):

    def __init__(self, host, port, out):
        self.out = out
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            log.debug('Incoming connection from %s' % repr(addr))
            handler = ScrawlHandlerSpoolDirTCP(sock, self.out)

class ScrawlScribeServer(asyncore.dispatcher):

    def __init__(self, host, port):
        log.debug('ScrawlScribeServer init ' + str(host) + ' ' + str(port))
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            log.debug('Incoming scribe connection from %s' % repr(addr))
            handler = ScrawlHandlerSpoolDirScribe(sock)

class ScrawlTCPClient():

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def send(self, category, message):
        success = None
        self.category = category
        self.message = message

        if debug: print('client_send category ' + str(category))
        if debug: print('client_send message ' + str(message))

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if debug: print('socket.time')
        try:
            if debug: print('scrawl protocol connecting to ' + self.host + ' port ' + str(self.port))
            server_address = (self.host, self.port)
            sock.connect(server_address)
        except socket.gaierror, e:
            print('Address-related error: ' % str(e) + ' ' + str(self.host) + ':' + str(self.port))
            return False
        except socket.error, e:
            print('Connection error: ' + str(e) + ' ' + str(self.host) + ':' + str(self.port))
            return False

        try:
            flag = '1'
            if debug: print('flag set to 1')

            catln = int(len(category))
            if debug: print('catln is ' + str(catln))

            catbyte = chr(catln)
            if debug: print('catbyte is ' + str(catbyte))

            msgln = int(len(message))
            if debug: print('msgln is ' + str(msgln))

            msgbyte1 = chr(int(math.floor(msgln / 256)))
            if debug: print('msgbyte1 is ' + str(msgbyte1) + '\r\n')

            msgbyte2 = chr(msgln % 256)
            if debug: print('msgbyte2 is ' + str(msgbyte2) + '\r\n')

            header = str(flag) + str(catbyte) + str(msgbyte1) + str(msgbyte2)
            if debug: print('scrawl header: ' + str(header))


            msg = str(header) + str(category) + str(message)
            if debug: print('sending scrawl msg')
            sock.sendall(msg)
            reply = sock.recv(1024)

            if not reply.startswith('6'):
                print('No ACK reply from server ' + str(reply))
                success = False
            else:
                if debug: print('scrawl ' + str(reply))
                success = True

            if debug: print('ScrawlTCPClient v2.5')

        finally:
            sock.close()
            if debug: print('sock.close')
            return success


class ScrawlSocketServerToScribe(asyncore.dispatcher):

    def __init__(self, path, scribe_host, scribe_port):
        self.path = path
        self.scribe_host = scribe_host
        self.scribe_port = scribe_port
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.set_reuse_addr()
        try:
            self.bind(self.path)
        except socket.error, e:
            log.debug(str(e) + ": " + str(self.path))
            sys.exit(1)

        # add backlog watch
        c = BackLogWatchScribe()
        t = multiprocessing.Process(target=c.run, args=(1, self.scribe_host, self.scribe_port))
        t.start()

        self.listen(5)

    def handle_accept(self):
        client = self.accept()
        if client is None:
            pass
        else:
            log.debug('Socket connection on %s' % str(self.path))
            log.debug('scribe_host ' + str(self.scribe_host) + ' scribe_port ' + str(self.scribe_port))
            handler = ScrawlHandlerSocketMemToScribe(*client, scribe_host=self.scribe_host, scribe_port=self.scribe_port)

class ScrawlSocketServerToScrawl(asyncore.dispatcher):

    def __init__(self, path, scrawl_host, scrawl_port):
        self.path = path
        self.scrawl_host = scrawl_host
        self.scrawl_port = scrawl_port
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.set_reuse_addr()
        try:
            self.bind(self.path)
        except socket.error, e:
            log.debug(str(e) + ": " + str(self.path))
            sys.exit(1)

        # add backlog watch
        c = BackLogWatchScrawl()
        t = multiprocessing.Process(target=c.run, args=(1, self.scrawl_host, self.scrawl_port))
        t.start()

        self.listen(5)

    def handle_accept(self):
        client = self.accept()
        if client is None:
            pass
        else:
            log.debug('Socket connection on %s' % str(self.path))
            log.debug('scrawl_host ' + str(self.scrawl_host) + ' scrawl_port ' + str(self.scrawl_port))
            handler = ScrawlHandlerSocketMemToScrawl(*client, scrawl_host=self.scrawl_host, scrawl_port=self.scrawl_port)


#class ScrawlHandlerSpoolDirSocket(asyncore.dispatcher_with_send):
#
#    def __init__(self, sock, addr):
#        asyncore.dispatcher_with_send.__init__(self, sock)
#        #self.buffer = 'scrawl'
#        self.buffer = 'Sty' #SOH chr(1)  (start of heading)
#        #self.buffer = '' #SOH chr(1)  (start of heading)
#
#    def handle_read(self):
#
#        data = self.recv(1024)
#        if data:
#            log.debug('getting data...')
#            log.debug(str(type(data)))
#
#            # v2.4
#            byte1 = flag = data[0:1]
#            byte2 = catsz1 = data[1:2]
#            byte3 = msgsz1 = data[2:3]
#            byte4 = msgsz2 = data[3:4]
#
#            #catsz = str(catsz1) + str(catsz2) + str(catsz3)
#            catsz = str(catsz1)
#            catsz_ord = ord(str(catsz1))
#
#            catsz_size = int(catsz)
#            log.debug('catsz_size ' + str(catsz_size))
#
#            category = data[4:catsz_size + 4]
#            log.debug('category: ' + category)
#
#            message = data[catsz_size + 4:-1]
#            log.debug('message: ' + message)
#
#            writedir = SPOOL + '/' + category
#            if not os.path.isdir(writedir):
#                os.mkdir(writedir, 0755 )
#                log.debug('mkdir ' + str(writedir))
#
#            writefile = writedir + '/' + category + '_00000'
#            with open(writefile, 'a') as wfile:
#                wfile.write(message + '\r\n')
#                log.debug('writefile ' + str(writefile))
#
#            response = 'OK'
#            self.send(response)
#            log.debug('send ' + str(response))
#
#            #log.debug("Drain")
#
#            #scribe.out
#            #drain1 = WatchFileTaskSyncToScribeRetryMem()
#            #t1 = threading.Thread(target=drain1.run, args=(writefile, 'localhost', 1463))
#            #t1.start()
#
#            #scrawl.out
#            #drain2 = WatchFileTaskSyncToScrawlTCPRetryMem()
#            #t2 = threading.Thread(target=drain2.run, args=(writefile, 'localhost', 9099))
#            #t2.start()
#
#            #scrawl+scribe.out
#            #drain3 = WatchFileTaskSyncToScribeAndScrawlTCPRetryMem()
#            #t3 = threading.Thread(target=drain3.run, args=(writefile, 'localhost', 1463, 'localhost', 9099))
#            #t3.start()
#
#            #print str('SendToScribeLoop')
#            #c1 = SendToScribeLoop()
#            #drain1 = threading.Thread(target=c1.run, args=(retryDict1, self.scribe_host, self.scribe_port, category, message ))
#            #drain1.start()
#
#            #scribe.out w/ timeout
#            #drain4 = WatchFileTaskSyncToScribeRetryMemTimeOut()
#            #t4 = threading.Thread(target=drain4.run, args=(writefile, 'localhost', 1463))
#            #t4.start()
#
#            #scribe.out w/ timeout and save back
#            #drain5 = WatchFileTaskSyncToScribeRetryMemTimeOutSaveBack()
#            #t5 = threading.Thread(target=drain5.run, args=(writefile, 'localhost', 1463))
#            #t5.start()
#
#            #scrawl.out w/ timeout and save back
#            drain6 = WatchFileTaskSyncToScrawlTCPRetryMemTimeOutSaveBack()
#            t6 = threading.Thread(target=drain6.run, args=(writefile, 'localhost', 9099))
#            t6.start()
#
#            #fire.1
#            #log.debug('No drain thread started')
#            return response
#
#    def writable(self):
#        return (len(self.buffer) > 0)
#
#    def handle_write(self):
#        self.send(self.buffer)
#        self.buffer = ''
#
#    def handle_close(self):
#        self.close()
#        log.debug("handle_close")

#############################################################################################################
class ScrawlHandlerSocketMemToScribe(asyncore.dispatcher_with_send):

    def __init__(self, sock, addr, scribe_host, scribe_port):
        self.scribe_host = scribe_host
        self.scribe_port = scribe_port
        asyncore.dispatcher_with_send.__init__(self, sock)
        #self.buffer = 'scrawl'
        self.buffer = '1' #SOH chr(1)  (start of heading)
        log.debug("ScrawlHandlerSocketMemToScribe")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            # v2.4
            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = msgsz1 = data[2:3]
            byte4 = msgsz2 = data[3:4]

            #catsz = str(catsz1) + str(catsz2) + str(catsz3)
            #catsz = str(catsz1)
            catsz_ord = ord(str(byte2))

            log.debug("catsz_ord  (cat_size) " + str(catsz_ord))

            #catsz_size = int(catsz)
            catsz_size = int(catsz_ord)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            # $msgbytes = chr(floor($msglen/256)) . chr($msglen%256);
            #msgln1 = ord(str(byte3))
            #msgln2 = ord(str(byte4))
            msglnstr = str(ord(str(byte3))) + str(ord(str(byte4)))
            log.debug('msgln str is ' + str(msglnstr))

            msgln = int(msglnstr)
            log.debug('msgln int is ' + str(msgln))
      
            #message = data[catsz_size + 4:-1]
            message = data[4 + catsz_size:4 + catsz_size + msgln]
            log.debug('message: ' + message)

            #writedir = SPOOL + '/' + category
            #if not os.path.isdir(writedir):
            #    os.mkdir(writedir, 0755 )
            #    log.debug('mkdir ' + str(writedir))
            #
            #writefile = writedir + '/' + category + '_00000'
            #with open(writefile, 'a') as wfile:
            #    wfile.write(message + '\r\n')
            #    log.debug('writefile ' + str(writefile))

            #response = 'OK'
            response = '6' #ACK chr(6) (acknowledge)
            self.send(response)
            log.debug('send ' + str(response))

            #fire.2 - scrawl tcp

            #scribe.out w/ timeout and save back
            #drain5 = WatchFileTaskSyncToScribeRetryMemTimeOutSaveBack()
            #t5 = threading.Thread(target=drain5.run, args=(writefile, 'localhost', 1463))
            #t5 = threading.Thread(target=drain5.run, args=(writefile, self.scribe_host, self.scribe_port))
            #t5.start()

            #drain 
            #scribe.out w/ timeout and save back
            drain = WatchMemTaskSyncToScribeRetryMemTimeOutSaveBack()
            t = threading.Thread(target=drain.run, args=(category, message, self.scribe_host, self.scribe_port))
            t.start()
            
            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.send(self.buffer)
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close")
#############################################################################################################

#############################################################################################################
class ScrawlHandlerSocketMemToScrawl(asyncore.dispatcher_with_send):

    def __init__(self, sock, addr, scrawl_host, scrawl_port):
        self.scrawl_host = scrawl_host
        self.scrawl_port = scrawl_port
        asyncore.dispatcher_with_send.__init__(self, sock)
        #self.buffer = 'scrawl'
        self.buffer = '1' #SOH chr(1)  (start of heading)
        log.debug("ScrawlHandlerSocketMemToScrawlTCP")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            # v2.4
            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = msgsz1 = data[2:3]
            byte4 = msgsz2 = data[3:4]

            catsz_ord = ord(str(byte2))

            log.debug("catsz_ord  (cat_size) " + str(catsz_ord))

            catsz_size = int(catsz_ord)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            msglnstr = str(ord(str(byte3))) + str(ord(str(byte4)))
            log.debug('msgln str is ' + str(msglnstr))

            msgln = int(msglnstr)
            log.debug('msgln int is ' + str(msgln))
      
            message = data[4 + catsz_size:4 + catsz_size + msgln]
            log.debug('message: ' + message)

            #response = 'OK'
            response = '6' #ACK chr(6) (acknowledge)
            self.send(response)
            log.debug('send ' + str(response))

            #drain 
            #scrawl.out w/ timeout and save back
            drain = WatchMemTaskSyncToScrawlRetryMemTimeOutSaveBack()
            t = threading.Thread(target=drain.run, args=(category, message, self.scrawl_host, self.scrawl_port))
            t.start()
            
            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.send(self.buffer)
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close scrawl_host")
#############################################################################################################

#############################################################################################################
class ScrawlHandlerSpoolDirSocketToScribe(asyncore.dispatcher_with_send):

    def __init__(self, sock, addr, scribe_host, scribe_port):
        self.scribe_host = scribe_host
        self.scribe_port = scribe_port
        asyncore.dispatcher_with_send.__init__(self, sock)
        #self.buffer = 'scrawl'
        self.buffer = '1' #SOH chr(1)  (start of heading)
        log.debug("ScrawlHandlerSpoolDirSocketToScribe")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            # v2.4
            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = msgsz1 = data[2:3]
            byte4 = msgsz2 = data[3:4]

            #catsz = str(catsz1) + str(catsz2) + str(catsz3)
            #catsz = str(catsz1)
            catsz_ord = ord(str(byte2))

            log.debug("catsz_ord  (cat_size) " + str(catsz_ord))

            #catsz_size = int(catsz)
            catsz_size = int(catsz_ord)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            # $msgbytes = chr(floor($msglen/256)) . chr($msglen%256);
            #msgln1 = ord(str(byte3))
            #msgln2 = ord(str(byte4))
            msglnstr = str(ord(str(byte3))) + str(ord(str(byte4)))
            log.debug('msgln str is ' + str(msglnstr))

            msgln = int(msglnstr)
            log.debug('msgln int is ' + str(msgln))
      
            #message = data[catsz_size + 4:-1]
            message = data[4 + catsz_size:4 + catsz_size + msgln]
            log.debug('message: ' + message)

            writedir = SPOOL + '/' + category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))

            writefile = writedir + '/' + category + '_00000'
            with open(writefile, 'a') as wfile:
                wfile.write(message + '\r\n')
                log.debug('writefile ' + str(writefile))

            #response = 'OK'
            response = '6' #ACK chr(6) (acknowledge)
            self.send(response)
            log.debug('send ' + str(response))

            #fire.2 - scrawl tcp

            #scribe.out w/ timeout and save back
            drain5 = WatchFileTaskSyncToScribeRetryMemTimeOutSaveBack()
            #t5 = threading.Thread(target=drain5.run, args=(writefile, 'localhost', 1463))
            t5 = threading.Thread(target=drain5.run, args=(writefile, self.scribe_host, self.scribe_port))
            t5.start()

            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.send(self.buffer)
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close")
#############################################################################################################


class ScrawlHandlerSpoolDirSocketToSrawl_v2_3(asyncore.dispatcher_with_send):

    def __init__(self, sock, addr, scrawl_host, scrawl_port):
        self.scrawl_host = scrawl_host
        self.scrawl_port = scrawl_port
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.buffer = 'scrawl'

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = catsz2 = data[2:3]
            byte4 = catsz3 = data[3:4]

            catsz = str(catsz1) + str(catsz2) + str(catsz3)

            catsz_size = int(catsz)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            message = data[catsz_size + 4:-1]
            log.debug('message: ' + message)

            writedir = SPOOL + '/' + category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))

            writefile = writedir + '/' + category + '_00000'
            with open(writefile, 'a') as wfile:
                wfile.write(message + '\r\n')
                log.debug('writefile ' + str(writefile))

            response = 'OK'
            self.send(response)
            log.debug('send ' + str(response))

            #fire.2 - scrawl tcp
            #scrawl.out w/ timeout and save back
            drain6 = WatchFileTaskSyncToScrawlTCPRetryMemTimeOutSaveBack()
            t6 = threading.Thread(target=drain6.run, args=(writefile, self.scrawl_host, self.scrawl_port))
            t6.start()

            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.send(self.buffer)
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close")


class ScrawlHandlerSpoolDirTCP(asyncore.dispatcher_with_send):
    #v2.5
    def __init__(self, sock, EVENTLOGS):
        self.EVENTLOGS = EVENTLOGS
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.buffer = '1'
        log.debug("ScrawlHandlerSpoolDirTCP v2.5")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = msgsz1 = data[2:3]
            byte4 = msgsz2 = data[3:4]

            catsz_ord = ord(str(byte2))

            log.debug("catsz_ord  (cat_size) " + str(catsz_ord))

            catsz_size = int(catsz_ord)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            msglnstr = str(ord(str(byte3))) + str(ord(str(byte4)))
            log.debug('msgln str is ' + str(msglnstr))

            msgln = int(msglnstr)
            log.debug('msgln int is ' + str(msgln))

            message = data[4 + catsz_size:4 + catsz_size + msgln]
            log.debug('message: ' + message)

            #print 'start'
            writedir = self.EVENTLOGS + '/' + category
            if not os.path.isdir(writedir):
                #print 'mkdir'
                try:
                    os.mkdir(writedir, 0755)
                    log.debug('mkdir ' + str(writedir))
                except OSError as e:
                    print 'Fail! mkdir ' + str(writedir)
                    print str(e)
                    sys.exit(1)

            writefile = writedir + '/' + category + '_00000'

            try:
                with open(writefile, 'a') as wfile:
                    wfile.write(message + '\r\n')
                    log.debug('writefile ' + str(writefile))
            except IOError as e:
                print 'Fail! write ' + str(writefile)
                print str(e)
                sys.exit(1)

            response = '6' #response = 'OK'
            self.send(response)
            log.debug('send ' + str(response))
            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close")

class ScrawlHandlerSpoolDirTCP_v2_4(asyncore.dispatcher_with_send):
    #v2.4

    def __init__(self, sock):
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.buffer = '1'
        log.debug("ScrawlHandlerSpoolDirTCP v2.4")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            #v2.3
            #byte1 = flag = data[0:1]
            #byte2 = catsz1 = data[1:2]
            #byte3 = catsz2 = data[2:3]
            #byte4 = catsz3 = data[3:4]
            #catsz = str(catsz1) + str(catsz2) + str(catsz3)
            #catsz_size = int(catsz)
            #log.debug('catsz_size ' + str(catsz_size))
            #
            #category = data[4:catsz_size + 4]
            #log.debug('category: ' + category)

            #message = data[catsz_size + 4:-1]
            #log.debug('message: ' + message)

            # v2.4
            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = msgsz1 = data[2:3]
            byte4 = msgsz2 = data[3:4]

            catsz_ord = ord(str(byte2))

            log.debug("catsz_ord  (cat_size) " + str(catsz_ord))

            catsz_size = int(catsz_ord)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            # $msgbytes = chr(floor($msglen/256)) . chr($msglen%256);
            #msgln1 = ord(str(byte3))
            #msgln2 = ord(str(byte4))
            msglnstr = str(ord(str(byte3))) + str(ord(str(byte4)))
            log.debug('msgln str is ' + str(msglnstr))

            msgln = int(msglnstr)
            log.debug('msgln int is ' + str(msgln))

            #message = data[catsz_size + 4:-1]
            message = data[4 + catsz_size:4 + catsz_size + msgln]
            log.debug('message: ' + message)

            #log.debug('category: ' + category)
            #log.debug('message: ' + message)

            writedir = EVENTLOGS + '/' + category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))

            writefile = writedir + '/' + category + '_00000'
            with open(writefile, 'a') as wfile:
                wfile.write(message + '\r\n')
                log.debug('writefile ' + str(writefile))

            #response = 'OK'
            response = '6'
            self.send(response)
            log.debug('send ' + str(response))
            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        #self.send(self.buffer)
        #self.send('OK')
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close")




#import fb303.FacebookService
#class Iface(fb303.FacebookService.Iface):
#  def Log(self, messages):
#    """
#    Parameters:
#     - messages
#    """
#    pass
#
#
#class ScrawlHandlerSpoolDirScribe_BETA2(asyncore.dispatcher_with_send, fb303.FacebookService.Client, Iface):
#    #scribe 
#
#
#    def __init__(self, sock, iprot, oprot=None):
#        asyncore.dispatcher_with_send.__init__(self, sock)
#        fb303.FacebookService.Client.__init__(self, iprot, oprot)
#
#        #from thrift.Thrift import *
#        import thrift
#        import fb303.FacebookService
#        #from ttypes import *
#        import ttypes
#        from thrift.Thrift import TProcessor
#        from thrift.transport import TTransport
#        from thrift.protocol import TBinaryProtocol
#        try:
#          from thrift.protocol import fastbinary
#        except:
#          fastbinary = None
#
#        self.buffer = 'Log'
#        log.debug("ScrawlHandlerSpoolDirSCRIBE THRIFT BETA2 BINARY PROTO")
#
#    def recv_Log(self, ):
#        (fname, mtype, rseqid) = self._iprot.readMessageBegin()
#        if mtype == TMessageType.EXCEPTION:
#          x = TApplicationException()
#          x.read(self._iprot)
#          self._iprot.readMessageEnd()
#          raise x
#        result = Log_result()
#        result.read(self._iprot)
#        self._iprot.readMessageEnd()
#        if result.success != None:
#          return result.success
#        raise TApplicationException(TApplicationException.MISSING_RESULT, "Log failed: unknown result");
#
#
#    def handle_read(self):
#        log.debug("handle_read")
#
#        data = self.recv(1024)
#        if data:
#            log.debug('getting data...')
#            log.debug(str(type(data)))
#
#            byte1  = data[0:1]
#            byte2  = data[1:2]
#            byte3  = data[2:3]
#            byte4  = data[3:4]
#
#            category = 'scribe_binary_protocol'
#
#            message = data
#            log.debug('category: ' + str(category))
#            log.debug('message: ' + str(message))
#
#            writedir = EVENTLOGS + '/' + category
#            if not os.path.isdir(writedir):
#                os.mkdir(writedir, 0755 )
#                log.debug('mkdir ' + str(writedir))
#
#            writefile = writedir + '/' + category + '_00000'
#            with open(writefile, 'a') as wfile:
#                wfile.write(message + '\r\n')
#                log.debug('writefile ' + str(writefile))
#
#            response = 'Log'
#            self.send(response)
#            log.debug('send ' + str(response))
#            return response
#
#    def writable(self):
#        return (len(self.buffer) > 0)
#
#    def handle_write(self):
#        #self.send(self.buffer)
#        #self.send('OK')
#        self.buffer = 'Log'
#
#    def handle_close(self):
#        self.close()
#        log.debug("handle_close")
#


class ScrawlHandlerSpoolDirScribe_v1(asyncore.dispatcher_with_send):
    #scribe attempt v1

    def __init__(self, sock):
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.buffer = 'Log'
        log.debug("ScrawlHandlerSpoolDirSCRIBE BETA2 BINARY PROTO")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            byte1  = data[0:1]
            byte2  = data[1:2]
            byte3  = data[2:3]
            byte4  = data[3:4]
            byte5  = data[4:5]
            byte6  = data[5:6]
            byte7  = data[6:7]
            byte8  = data[7:8]
            byte9  = data[8:9]
            byte10 = data[9:10]
            byte11 = data[10:11]
            byte12 = data[11:12]
            byte13 = data[12:13]
            byte14 = data[13:14]
            byte15 = data[14:15]
            byte16 = data[15:16]

            byte17 = data[16:17]
            byte18 = data[17:18]
            byte19 = data[18:19]
            byte20 = data[19:20]
            byte21 = data[20:21]
            byte22 = data[21:22]
            byte23 = data[22:23]
            byte24 = data[23:24]
            byte25 = data[24:25]
            byte26 = data[25:26]
            byte27 = data[26:27]
            byte28 = data[27:27]
            byte29 = data[28:29]
            byte30 = data[29:30]
            byte31 = data[30:31]
            byte32 = data[31:32]


            log.debug('byte1 ' + str(byte1))
            log.debug('byte2 ' + str(byte2))
            log.debug('byte3 ' + str(ord(str(byte3))))
            log.debug('byte4 ' + str(ord(str(byte4))))
            log.debug('byte5 ' + str(byte5))
            log.debug('byte6 ' + str(byte6))
            log.debug('byte7 ' + str(byte7))
            log.debug('byte8 ' + str(byte8))
            log.debug('byte9 ' + str(byte9))
            log.debug('byte10 ' + str(byte10))
            log.debug('byte11 ' + str(byte11))
            log.debug('byte12 ' + str(byte12))
            log.debug('byte13 ' + str(byte13))
            log.debug('byte14 ' + str(byte14))
            log.debug('byte15 ' + str(byte15))
            log.debug('byte16 ' + str(byte16))
            log.debug('byte17 ' + str(byte17))
            log.debug('byte18 ' + str(byte18))
            log.debug('byte19 ' + str(byte19))
            log.debug('byte20 ' + str(byte20))
            log.debug('byte21 ' + str(byte21))
            log.debug('byte22 ' + str(byte22))
            log.debug('byte23 ' + str(byte23))
            log.debug('byte24 ' + str(byte24))
            log.debug('byte25 ' + str(byte25))
            log.debug('byte26 ' + str(byte26))
            log.debug('byte27 ' + str(byte27))
            log.debug('byte28 ' + str(byte28))
            log.debug('byte29 ' + str(byte29))
            log.debug('byte30 ' + str(byte30))
            log.debug('byte31 ' + str(byte31))
            log.debug('byte32 ' + str(byte32))

            byte64 = data[63:64]
            log.debug('byte64 ' + str(byte64))

            byte65 = data[64:65]
            log.debug('byte65 ' + str(byte65))

            category = 'scribe_binary_protocol'

            message = data
            log.debug('category: ' + str(category))
            log.debug('message: ' + str(message))

            writedir = EVENTLOGS + '/' + category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))

            writefile = writedir + '/' + category + '_00000'
            with open(writefile, 'a') as wfile:
                wfile.write(message + '\r\n')
                log.debug('writefile ' + str(writefile))

            #response = 'Log'
            #response = '6'
            response = '^@^@^@2^@^@^@^CLog^A^@^@^@^@^O^@^A^L^@^@^@^A^K^@^A^@^@^@^G'
            response += '                                                        D'
            self.send(response)
            log.debug('send ' + str(response))
            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        #self.send(self.buffer)
        #self.send('OK')
        self.buffer = 'Log'

    def handle_close(self):
        self.close()
        log.debug("handle_close")



class ScrawlHandlerSpoolDirTCP_v2_3(asyncore.dispatcher_with_send):

    def __init__(self, sock):
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.buffer = 'scrawl'
        log.debug("ScrawlHandlerSpoolDirTCP v2.3")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = catsz2 = data[2:3]
            byte4 = catsz3 = data[3:4]

            catsz = str(catsz1) + str(catsz2) + str(catsz3)

            catsz_size = int(catsz)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            message = data[catsz_size + 4:-1]
            log.debug('message: ' + message)

            log.debug('category: ' + category)
            log.debug('message: ' + message)

            writedir = EVENTLOGS + '/' + category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))

            writefile = writedir + '/' + category + '_00000'
            with open(writefile, 'a') as wfile:
                wfile.write(message + '\r\n')
                log.debug('writefile ' + str(writefile))

            response = 'OK'
            self.send(response)
            log.debug('send ' + str(response))
            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        #self.send(self.buffer)
        #self.send('OK')
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close")


class ScrawlHandlerSpoolFile(asyncore.dispatcher_with_send):

    def __init__(self, sock, addr):
        #self.spool = spool
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.addr = addr
        self.buffer = 'scrawl'

    def handle_read(self):
        data = self.recv(1024)
        if data:
            #print str(data)
            #pass
            try:
                writefile = SPOOL + '/scrawl_spool_file00000'
                with open(writefile, 'a') as wfile:
                    wfile.write(data + '\r\n')
                    log.debug('writefile ' + str(writefile))
            except IOError, e:
                print str(e)
                sys.exit(1)

            response = 'OK'
            self.send(response)

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.send(self.buffer)
        self.buffer = ''

    def handle_close(self):
        self.close()

#server = ScrawlServer()
#try:
#    asyncore.loop()
#finally:
#    if os.path.exists(path):
#        os.unlink(path)
#

class ScrawlSocketClient():

    def __init__(self, path):
        self.path = path

    def is_socket(self, path):
        isSocket = False
        #print 'path: ' + str(path)
        if os.path.exists(path):
            mode = os.stat(path).st_mode
            #import stat
            isSocket = stat.S_ISSOCK(mode)
            #isSocket = True
            #print "%s is socket: %s" % (path, isSocket)
        else:
            isSocket = False
        return isSocket

    def send(self, category, message):
        self.category = category
        self.message = message

        flag = '1' #SOH chr(1) (start of heading)
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

        if self.is_socket(self.path) == False:
            print 'Socket Error: No such file or directory ' + str(self.path)
            return False
    
        try:
            log.debug('connecting to ' + str(self.path))
            sock.connect(self.path)
        except socket.error, e:
            #log.critical("Error: %s" % str(e))
            print('Socket Error: ' + str(e) + ' ' + str(self.path))
            #sys.exit(1)
            return False

        #v2.3
        #catsz = str(len(category))
        #if len(catsz) < 3:
        #    #print 'not 3'
        #    catsz = catsz.zfill(3)

        #v2.4
        catln = int(len(category))
        log.debug('catln is ' + str(catln)) 

        catbyte = chr(catln)
        log.debug('catbyte is ' + str(catbyte)) 

        msgln = int(len(message))
        log.debug('msgln is ' + str(msgln))

        msgbyte1 = chr(int(math.floor(msgln / 256)))
        log.debug('msgbyte1 is ' + str(msgbyte1) + '\r\n')

        msgbyte2 = chr(msgln % 256)
        log.debug('msgbyte2 is ' + str(msgbyte2) + '\r\n')

        #msgbytes = str(chr(int(math.floor(msgln / 256)))) + str(chr(msgln % 256))
        #log.debug('msgbytes is ' + str(msgbytes) + '\r\n')
        
        header = str(flag) + str(catbyte) + str(msgbyte1) + str(msgbyte2)
        log.debug('scrawl header: ' + str(header))

        con = sock.recv(1)
        #if not con.startswith('scrawl'):
        if not con.startswith('1'):
            log.critical('No scrawl header from server')
            sys.exit(1)
            #return False

        msg = str(header) + str(category) + str(message)
        #log.debug('sending: ' + str(msg))
        log.debug('sending message')
        sock.sendall(msg)
        reply = sock.recv(1)
        if not reply.startswith('6'): #ACK chr(6) (acknowledge)
            log.critical('No ACK reply from server ' + str(reply))
            success = False
        else:
            log.debug(str(reply))
            success = True

        return success
   

class ScrawlSocketClient_v2_3():

    def __init__(self, path):
        self.path = path

    def send(self, category, message):
        self.category = category
        self.message = message

        flag = 'A'
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    
        try:
            log.info('connecting to ' + str(self.path))
            sock.connect(self.path)
        except socket.error, e:
            log.critical("Error: %s" % str(e))
            sys.exit(1)

        catsz = str(len(category))
        if len(catsz) < 3:
            #print 'not 3'
            catsz = catsz.zfill(3)

        header = str(flag) + str(catsz)
        log.debug('scrawl header: ' + str(header))
        #print 'header ' + str(header)

        con = sock.recv(1024)
        if not con.startswith('scrawl'):
            log.critical('No scrawl header from server')
            sys.exit(1)
            #return False

        msg = str(header) + str(category) + str(message)
        #log.debug('sending: ' + str(msg))
        log.debug('sending message')
        sock.sendall(msg)
        reply = sock.recv(1024)
        #print str(reply)
        if not reply.startswith('OK'):
            log.critical('No OK reply from server ' + str(reply))
            success = False
        else:
            log.info(str(reply))
            success = True

        return success


   

class ScrawlTCPClient_v2_4():
    # v2.4 header

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def send(self, category, message):
        success = None
        self.category = category
        self.message = message

        log.debug('client_send category ' + str(category))
        log.debug('client_send message ' + str(message))

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            log.info('scrawl protocol connecting to ' + self.host + ' port ' + str(self.port))
            server_address = (self.host, self.port)
            sock.connect(server_address)
        except socket.gaierror, e:
            log.critical("scrawl Address-related error connecting to server: %s" % str(e))
            sys.exit(1)
        except socket.error, e:
            #log.critical("scrawl Connection error: %s" % str(e))
            log.debug("scrawl Connection error: %s" % str(e))
            sys.exit(1)
            #logging.1

        #sock.send('')
        #con = sock.recv(1024)
        #if not con.startswith('scrawl'):
        #    log.critical('No scrawl header from server')
        #    sys.exit(1)
        #    #return False
        #con = sock.recv(16)
        #print 'response ' + str(con)

        # fix.me.fix.me.v2.4.mark.2
        try:
            flag = '1'

           
            #v2.3 
            #catsz = str(len(category))
            #if len(catsz) < 3:
                #print 'not 3'
                #catsz = catsz.zfill(3)
  
            #v2.4
            catln = int(len(category))
            log.debug('catln is ' + str(catln))

            catbyte = chr(catln)
            log.debug('catbyte is ' + str(catbyte))

            msgln = int(len(message))
            log.debug('msgln is ' + str(msgln))

            msgbyte1 = chr(int(math.floor(msgln / 256)))
            log.debug('msgbyte1 is ' + str(msgbyte1) + '\r\n')

            msgbyte2 = chr(msgln % 256)
            log.debug('msgbyte2 is ' + str(msgbyte2) + '\r\n')

            #msgbytes = str(chr(int(math.floor(msgln / 256)))) + str(chr(msgln % 256))
            #log.debug('msgbytes is ' + str(msgbytes) + '\r\n')

            header = str(flag) + str(catbyte) + str(msgbyte1) + str(msgbyte2)
            log.debug('scrawl header: ' + str(header))


            msg = str(header) + str(category) + str(message)
            #if debug: log.debug('sending ' + str(msg))
            log.debug('sending scrawl msg')
            sock.sendall(msg)
            reply = sock.recv(1024)
            #print str(reply)
            #if not reply.startswith('OK'): #v2.3
            if not reply.startswith('6'):
                log.debug('No ACK reply from server ' + str(reply))
                success = False
            else:
                log.info('scrawl ' + str(reply))
                success = True

            log.debug('Log.TCP.v2.4')

            #response = sock.recv(1024)
            #if debug: log.debug('received ' + response)
            #except socket.error, e:
            #if debug: log.debug("Error sending data: %s" % str(e))

        finally:
            sock.close()
            log.debug('sock.close')
            return success

        #log.debug('Log.TCP')

   
class ScrawlTCPClient_v2_3():
    # v2.3 header

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def send(self, category, message):
        success = None
        self.category = category
        self.message = message

        log.debug('client_send category ' + str(category))
        log.debug('client_send message ' + str(message))

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            log.info('scrawl protocol connecting to ' + self.host + ' port ' + str(self.port))
            server_address = (self.host, self.port)
            sock.connect(server_address)
        except socket.gaierror, e:
            log.critical("scrawl Address-related error connecting to server: %s" % str(e))
            sys.exit(1)
        except socket.error, e:
            log.critical("scrawl Connection error: %s" % str(e))
            sys.exit(1)
            #logging.1

        #sock.send('')
        #con = sock.recv(1024)
        #if not con.startswith('scrawl'):
        #    log.critical('No scrawl header from server')
        #    sys.exit(1)
        #    #return False
        #con = sock.recv(16)
        #print 'response ' + str(con)

        try:
            flag = 'A'
            catsz = str(len(category))
            if len(catsz) < 3:
                #print 'not 3'
                catsz = catsz.zfill(3)

            header = str(flag) + str(catsz)
            log.debug('scrawl header: ' + str(header))

            msg = str(header) + str(category) + str(message)
            #if debug: log.debug('sending ' + str(msg))
            log.debug('sending scrawl msg')
            sock.sendall(msg)
            reply = sock.recv(1024)
            #print str(reply)
            if not reply.startswith('OK'):
                log.debug('No OK reply from server ' + str(reply))
                success = False
            else:
                log.info('scrawl ' + str(reply))
                success = True

            log.debug('Log.TCP')

            #response = sock.recv(1024)
            #if debug: log.debug('received ' + response)
            #except socket.error, e:
            #if debug: log.debug("Error sending data: %s" % str(e))

        finally:
            sock.close()
            log.debug('sock.close')
            return success

        #log.debug('Log.TCP')

 

class ScribeClient():

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def send(self, category, message):
        result = None
        self.category = category
        self.message = message

        # yum install scribe-python
        # yum install python-thrift

        from scribe import scribe
        from thrift.transport import TTransport, TSocket
        from thrift.protocol import TBinaryProtocol

        log_entry = scribe.LogEntry(category, message)
        # depending on thrift version
        #   log_entry = scribe.LogEntry(dict(category=category, message=message))

        log.info('scribe protocol connecting ' + (self.host) + ' port ' + str(self.port))
        socket = TSocket.TSocket(host=self.host, port=self.port)
        transport = TTransport.TFramedTransport(socket)
        protocol = TBinaryProtocol.TBinaryProtocol(trans=transport, strictRead=False, strictWrite=False)
        client = scribe.Client(iprot=protocol, oprot=protocol)

        transport.open()
        result = client.Log(messages=[log_entry])
        transport.close()

        if result == 0:
            log.info('scribe success ' + str(result))
        else:
            log.info('Error result ' + str(result))

        return result

class ScrawlSpoolMemFileToScrawlTCPRetryMemTimeOutSaveBack():
    # dir format storage.

    def __init__(self, category, message, host, port):
        self.category = category
        self.message = message
        self.host = host
        self.port = port
        log.debug('category ' + str(category))
        log.debug('message ' + str(message))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))
        log.debug('ScrawlSpoolMemFileToScrawlTCPRetryMemTimeOutSaveBack')

    def send(self):

        log.debug('category :' + self.category)
        log.debug('message :' + self.message)

        drainDict = {}
        drainDict[self.category] = self.message

        TIMEOUT = 1
        result = None
        while TIMEOUT > 0:
            log.debug('TTS-minus scrawl ' + str(TIMEOUT))
            client = ScrawlTCPClient(self.host, self.port)
            try:
                result = client.send(self.category, self.message)
                log.debug('ScrawlTCPClient retun ' + str(result))
            except:
                #party.foul
                #log.warning('scrawl TTransportException ' + str(result))
                log.warning('scrawl error/return ' + str(result))

            if result is None:
                log.debug('Scrawl Client return FAIL ' + str(result))
                #del drainDict[self.category]
                #break
            if result is True:
                log.debug('Scrawl Client return SUCCESS ' + str(result))
                del drainDict[self.category]
                break
            TIMEOUT -= 1
        #time.sleep(1)
        # fix.me.fix.me.now

        if not drainDict.keys():
            log.debug("DELIVERED scrawl TCP v2.4 ")
        else:
            log.debug("WRITE BACKLOG scrawl TCP v2.4")
            backlogdir = SPOOL + '/backlog'
            if not os.path.isdir(backlogdir):
                os.mkdir(backlogdir, 0755 )
                log.debug('mkdir ' + str(backlogdir))
        
            writedir = backlogdir + '/' + self.category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))
            writefile = writedir + '/' + self.category + '_00000'
        
            with open(writefile, 'a') as fout:
                fout.write(self.message + '\r\n')
                log.debug('writefile ' + str(writefile))

        return result
##############################################################################
        
class ScrawlSpoolMemFileToScribeRetryMemTimeOutSaveBack():
    # dir format storage.

    def __init__(self, category, message, host, port):
        self.category = category
        self.message = message
        self.host = host
        self.port = port
        log.debug('category ' + str(category))
        log.debug('message ' + str(message))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))
        log.debug('ScrawlSpoolMemFileToScribeRetryMemTimeOutSaveBack')

    def send(self):

        log.debug('category :' + self.category)
        log.debug('message :' + self.message)

        drainDict = {}
        drainDict[self.category] = self.message

        #TIMEOUT = 3
        TIMEOUT = 1
        result = None
        while TIMEOUT > 0:
            log.debug('TTS-minus scribe ' + str(TIMEOUT))
            client = ScribeClient(self.host, self.port)
            try:
                result = client.send(self.category, self.message)
            except:
                log.warning('scribe TTransportException ' + str(result))

            if result == 0:
                log.debug('ScribeClient return ' + str(result))
                del drainDict[self.category]
                break
            TIMEOUT -= 1
        #time.sleep(1)

        if not drainDict.keys():
            log.debug("DELIVERED")
        else:
            log.debug("WRITE BACKLOG")
            backlogdir = SPOOL + '/backlog'
            if not os.path.isdir(backlogdir):
                os.mkdir(backlogdir, 0755 )
                log.debug('mkdir ' + str(backlogdir))
        
            writedir = backlogdir + '/' + self.category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))
            writefile = writedir + '/' + self.category + '_00000'
        
            with open(writefile, 'a') as fout:
                fout.write(self.message + '\r\n')
                log.debug('writefile ' + str(writefile))

        return result
##############################################################################
        
class ScrawlSpoolDirFileToScribe():
    # dir format storage.

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                fin.truncate(0)
                log.debug('TRUNCATE file ' + str(self.path))
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        log.debug(' get cat from ' + self.path)

        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
  
        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl


        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        count = 0
        for message in mem_data:
            count += 1
            log.debug('message ' + str(count))
            log.debug('message ' + str(message))

            from scribe import scribe
            from thrift.transport import TTransport, TSocket
            from thrift.protocol import TBinaryProtocol

            log_entry = scribe.LogEntry(category, message)
            # depending on thrift version
            #   log_entry = scribe.LogEntry(dict(category=category, message=message))

            socket = TSocket.TSocket(host=self.host, port=self.port)
            transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocol(trans=transport, strictRead=False, strictWrite=False)
            client = scribe.Client(iprot=protocol, oprot=protocol)

            result = None
            try:
                transport.open()
                result = client.Log(messages=[log_entry])
            except:
                log.warning('Warning on result ' + str(result))
            transport.close()

            if result == 0:
                #with open(spoolfile, 'w') as fout:
                #    fout.writelines(data[1:])
                log.info('success1 ' + str(result))
                #fin.truncate(0)
            else:
                log.critical('Error result ' + str(result))

class ScrawlSpoolDirFileToScribeRetryMemTimeOut():
    # dir format storage.

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                fin.truncate(0)
                log.debug('TRUNCATE file ' + str(self.path))
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        log.debug(' get cat from ' + self.path)

        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])

        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl

        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        #with open(self.path, 'r+') as fin:
        #    mem_data = fin.read().splitlines(True)
        #    fin.truncate(0)

        drainDict = {}
        count = 0
        for message in mem_data:
            count += 1
            drainDict[count] = message

        #re.work.1
        TIMEOUT = 3
        result = None
        n = len(drainDict.keys())
        while drainDict and TIMEOUT > 0:
            log.debug('TT-minus scribe ' + str(TIMEOUT))
            for k in list(drainDict.keys()):
                client = ScribeClient(self.host, self.port)
                try:
                    message = drainDict[k]
                    result = client.send(category, message)
                except:
                    log.warning('scribe TTransportException ' + str(result))
                    #log.critical('scribe TTransportException ')

                if result == 0:
                    log.debug('ScribeClient return ' + str(result))
                    n -= 1
                    del drainDict[k]
            TIMEOUT -= 1 
            time.sleep(1)        

        return result

class ScrawlSpoolDirFileToScrawlRetryMemTimeOutSaveBack():
    # dir format storage.

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                log.debug('REMOVE file ' + str(self.path))
                os.remove(self.path)
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        log.debug(' get cat from ' + self.path)

        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl

        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        drainDict = {}
        count = 0
        for message in mem_data:
            count += 1
            drainDict[count] = message

        TIMEOUT = 3
        result = None
        n = len(drainDict.keys())
        while drainDict and TIMEOUT > 0:
            log.debug('TTS-minus scrawl TCP delivery... ' + str(TIMEOUT))
            for k in list(drainDict.keys()):
                client = ScrawlTCPClient(self.host, self.port)
                try:
                    message = drainDict[k]
                    result = client.send(category, message)
                except:
                    #party.foul
                    #log.warning('scribe TTransportException ' + str(result))
                    log.warning('scrawl TCP result ' + str(result))

                if result is True:
                    log.debug('ScrawlClient return ' + str(result))
                    n -= 1
                    del drainDict[k]
            TIMEOUT -= 1
            time.sleep(1)

        #unload drainDict
        #for k in list(drainDict.keys()):
        #    open('/tmp/out', 'a').write(drainDict[k])
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])

        backlogdir = SPOOL + '/backlog'
        if not os.path.isdir(backlogdir):
            os.mkdir(backlogdir, 0755 )
            log.debug('mkdir ' + str(backlogdir))

        writedir = backlogdir + '/' + category
        if not os.path.isdir(writedir):
            os.mkdir(writedir, 0755 )
            log.debug('mkdir ' + str(writedir))
        writefile = writedir + '/' + category + '_00000'

        for k in list(drainDict.keys()):
            with open(writefile, 'a') as fout:
                #fout.write(message + '\r\n')
                fout.write(drainDict[k])
                log.debug('writefile ' + str(writefile))

        return result
##############################################################################

##############################################################################
class ScrawlSpoolDirFileToScribeRetryMemTimeOutSaveBack():
    # dir format storage.

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                #fin.truncate(0)
                #log.debug('TRUNCATE file ' + str(self.path))
                log.debug('REMOVE file ' + str(self.path))
                os.remove(self.path)
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        log.debug(' get cat from ' + self.path)

        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])

        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl

        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        #with open(self.path, 'r+') as fin:
        #    mem_data = fin.read().splitlines(True)
        #    fin.truncate(0)

        drainDict = {}
        count = 0
        for message in mem_data:
            count += 1
            drainDict[count] = message

        #re.work.2
        TIMEOUT = 3
        result = None
        n = len(drainDict.keys())
        while drainDict and TIMEOUT > 0:
            log.debug('TTS-minus scribe ' + str(TIMEOUT))
            for k in list(drainDict.keys()):
                client = ScribeClient(self.host, self.port)
                try:
                    message = drainDict[k]
                    result = client.send(category, message)
                except:
                    log.warning('scribe TTransportException ' + str(result))
                    #log.critical('scribe TTransportException ')
                    # this hangs a long return 
                    # INFO scribe protocol connecting 192.168.2.15 port 1463

                if result == 0:
                    log.debug('ScribeClient return ' + str(result))
                    n -= 1
                    del drainDict[k]
            TIMEOUT -= 1
            time.sleep(1)

        #unload drainDict
        #for k in list(drainDict.keys()):
        #    open('/tmp/out', 'a').write(drainDict[k])
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])

        backlogdir = SPOOL + '/backlog'
        if not os.path.isdir(backlogdir):
            os.mkdir(backlogdir, 0755 )
            log.debug('mkdir ' + str(backlogdir))

        writedir = backlogdir + '/' + category
        if not os.path.isdir(writedir):
            os.mkdir(writedir, 0755 )
            log.debug('mkdir ' + str(writedir))
        writefile = writedir + '/' + category + '_00000'

        for k in list(drainDict.keys()):
            with open(writefile, 'a') as fout:
                #fout.write(message + '\r\n')
                fout.write(drainDict[k])
                log.debug('writefile ' + str(writefile))

        return result
##############################################################################



class ScrawlSpoolDirFileToScribeRetryMem():
    # dir format storage.

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                fin.truncate(0)
                log.debug('TRUNCATE file ' + str(self.path))
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        log.debug(' get cat from ' + self.path)

        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])

        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl


        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        #with open(self.path, 'r+') as fin:
        #    mem_data = fin.read().splitlines(True)
        #    fin.truncate(0)


        retryDict = {}
        count = 0
        for message in mem_data:
            count += 1
            log.debug('message ' + str(count))
            log.debug('message ' + str(message))

            from scribe import scribe
            from thrift.transport import TTransport, TSocket
            from thrift.protocol import TBinaryProtocol

            log_entry = scribe.LogEntry(category, message)
            # depending on thrift version
            #   log_entry = scribe.LogEntry(dict(category=category, message=message))

            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        log.debug(' get cat from ' + self.path)

        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])

        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl


        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        retryDict = {}
        count = 0
        for message in mem_data:
            count += 1
            log.debug('message ' + str(count))
            log.debug('message ' + str(message))

            from scribe import scribe
            from thrift.transport import TTransport, TSocket
            from thrift.protocol import TBinaryProtocol

            log_entry = scribe.LogEntry(category, message)
            # depending on thrift version
            #   log_entry = scribe.LogEntry(dict(category=category, message=message))
            socket = TSocket.TSocket(host=self.host, port=self.port)
            transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocol(trans=transport, strictRead=False, strictWrite=False)
            client = scribe.Client(iprot=protocol, oprot=protocol)

            result = None
            try:
                transport.open()
                result = client.Log(messages=[log_entry])
            except:
                log.warning('Warning on result')
            transport.close()

            if result == 0:
                log.info('success1 ' + str(result))
            else:
                log.critical('Error result ' + str(result))
                retryDict[count] = message

        while retryDict:
            try:
                for key, item in retryDict.iteritems():
                    print 'retry ' + str(item)
                    #client = ScrawlTCPClient(self.host, self.port)
                    client = scribe.Client(iprot=protocol, oprot=protocol)
                    try:
                        #result = client.send(category, message)
                        transport.open()
                        result = client.Log(messages=[log_entry])
                    except:
                        log.critical('log.critical ' + str(result))

                    if result != 0:
                        log.debug('Not OK.  queued message ...')
                        time.sleep(1)
                    else:
                        log.debug('ClientEnd ' + str(result))
                        del retryDict[key]
            except RuntimeError as e:
                log.debug('RuntimeError ' + str(e))
                if 'dictionary changed size during iteration' in e:
                    break

        return result


class ScrawlSpoolDirFileToScrawlTCPRetryMem():
    # dir format storage.
    # read-in entire file, truncate

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        success = None
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                fin.truncate(0)
                log.debug("TRUNCATE " + str(self.path))
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        log.debug(' get cat from ' + self.path)

        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])

        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl

        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        #working.1
        count = 0
        retryDict = {}
        #retryList = []
        for message in mem_data:
            result = None
            count += 1
            log.debug('message ' + str(count))
            log.debug('message ' + str(message))

            #client = scribe.Client(iprot=protocol, oprot=protocol)
            print 'ScrawlTCPClient'
            #client = ScrawlTCPClient('localhost', 9099)
            log.debug('scrawl_server ' + str(self.host) + ' scrawl_port ' + str(self.port))
            client = ScrawlTCPClient(self.host, self.port)
            try:
                result = client.send(category, message) 
            except:
                log.critical('log.critical ' + str(result))

            print 'RESULT ' + str(result)

            if result != True:
                log.debug('Not OK.  queue store message ...')
                retryDict[count] = message
                success = False
                #retryList.extend(message)
            else:
                log.debug('ClientEnd ' + str(result))
                success = True
       
        while retryDict:
            try:
                for key, item in retryDict.iteritems():
                    print 'retry ' + str(item)
                    client = ScrawlTCPClient(self.host, self.port)
                    try:
                        result = client.send(category, message)
                    except:
                        log.critical('log.critical ' + str(result))
        
                    if result != True:
                        log.debug('Not OK.  queued message ...')
                        time.sleep(1)
                    else:
                        log.debug('ClientEnd ' + str(result))
                        del retryDict[key]
            except RuntimeError as e:
                log.debug('RuntimeError ' + str(e))
                if 'dictionary changed size during iteration' in e:
                    break

        return success   


class ScrawlSpoolDirFileToScrawlTCPRetryMemTimeOutSaveBack():
    # dir format storage.
    # read-in entire file, truncate
    # save-back

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        success = None
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                #fin.truncate(0)
                #log.debug("TRUNCATE " + str(self.path))
                log.debug("REMOVE " + str(self.path))
                os.remove(self.path)
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        log.debug(' get cat from ' + self.path)

        osdir1  = os.path.dirname(self.path)
        osdir2  = os.path.dirname(os.path.dirname(self.path))

        log.debug(self.path + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl

        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        #working.2
        count = 0
        drainDict = {}
        #retryList = []
        for message in mem_data:
            count += 1
            drainDict[count] = message

        TIMEOUT = 3
        result = None
        n = len(drainDict.keys())
        while drainDict and TIMEOUT > 0:
            log.debug('TTS-minus scrawl ' + str(TIMEOUT))
            for k in list(drainDict.keys()):
                #client = ScribeClient(self.host, self.port)
                client = ScrawlTCPClient(self.host, self.port)
                try:
                    message = drainDict[k]
                    result = client.send(category, message)
                except:
                    #log.warning('scribe TTransportException ' + str(result))
                    #log.critical('scribe TTransportException ')
                    log.critical('log.critical ' + str(result))
                log.debug('RESULT is ' + str(result))
                #if result == 'OK':
                if result == True:
                    n -= 1
                    del drainDict[k]
                    log.debug('scrawl OK')
                else:
                    #log.debug('ScribeClient return ' + str(result))
                    log.debug('Not OK.  scrawl queue store message ...')
            TIMEOUT -= 1
            time.sleep(1)

        #write backlog
        backlogdir = SPOOL + '/backlog'
        if not os.path.isdir(backlogdir):
            os.mkdir(backlogdir, 0755 )
            log.debug('mkdir ' + str(backlogdir))

        writedir = backlogdir + '/' + category
        if not os.path.isdir(writedir):
            os.mkdir(writedir, 0755 )
            log.debug('mkdir ' + str(writedir))
        writefile = writedir + '/' + category + '_00000'

        for k in list(drainDict.keys()):
            with open(writefile, 'a') as fout:
                #fout.write(message + '\r\n')
                fout.write(drainDict[k])
                log.debug('writefile ' + str(writefile))


        return result

class ScrawlSpoolDirFileToScribeAndScrawlTCPRetryMem():
    # dir format storage.

    def __init__(self, path, scribe_host, scribe_port, scrawl_host, scrawl_port):
        self.path = path
        self.scribe_host = scribe_host
        self.scribe_port = scribe_port
        self.scrawl_host = scrawl_host
        self.scrawl_port = scrawl_port
        log.debug('path ' + str(path))
        log.debug('scribe_host ' + str(scribe_host))
        log.debug('scribe_port ' + str(scribe_port))
        log.debug('scrawl_host ' + str(scrawl_host))
        log.debug('scrawl_port ' + str(scrawl_port))

    def runScribe(self, host, port, category, message):
        self.host = host
        self.port = port
        self.category = category
        self.message = message
        client = ScribeClient(host, port)
        result = client.send(category, message)
        return result

    def runScrawlTCP(self, host, port, category, message):
        self.host = host
        self.port = port
        self.category = category
        self.message = message
        client = ScrawlTCPClient(host, port)
        result = client.send(category, message)
        return result
          

    def send(self):
        result = None
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                fin.truncate(0)
                log.debug('TRUNCATE file ' + str(self.path))
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        log.debug('ScrawlSpoolDirFileToScribeAndScrawlTCPRetryMem get cat from ' + self.path)

        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])
        #infile = os.path.join(os.getcwd(), os.listdir(os.getcwd())[0])

        #infile = self.path
        osdir1  = os.path.dirname(self.path)
        osdir2  = os.path.dirname(os.path.dirname(self.path))

        log.debug(self.path + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl


        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))


        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        retryDict1 = {}
        retryDict2 = {}
        count = 0
        for message in mem_data:
            count += 1
            retryDict1[count] = message
            retryDict2[count] = message

        #mark.1
        #print str('SendToScribeLoop')
        c1 = SendToScribeLoop()
        drain1 = threading.Thread(target=c1.run, args=(retryDict1, self.scribe_host, self.scribe_port, category, message ))
        drain1.start()

        #mark.2
        #print str('SendToScrawlLoop')
        c2 = SendToScrawlLoop()
        drain2 = threading.Thread(target=c2.run, args=(retryDict2, self.scrawl_host, self.scrawl_port, category, message ))
        drain2.start()

        return result


class ScrawlSpoolFileToScribe():
    # single file with header text layout

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                fin.truncate(0)
                log.debug('TRUNCATE file ' + str(self.path))
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)
      
        count = 0 
        for item in mem_data:
            count += 1
            log.debug('item ' + str(count))

            byte1 = flag = first_line[0:1]
            byte2 = catsz1 = first_line[1:2]
            byte3 = catsz2 = first_line[2:3]
            byte4 = catsz3 = first_line[3:4]

            catsz = str(catsz1) + str(catsz2) + str(catsz3)
            #print str(catsz)
            #print str(byte1)
            #print str(byte2)
            #print str(byte3)
            #print str(byte4)

            catsz_size = int(catsz)
            #print 'catsz_size ' + str(catsz_size)

            category = first_line[4:catsz_size + 4]
            #print str('category: ' + category)

            message = first_line[catsz_size + 4:-1]
            #print str('message: ' + message)

            #client = ScribeClient(self.host, self.port)
            #client.send(category, message) 

            #with open(self.path, 'w') as fout:
            #    fout.writelines(mem_data[1:])
    
            from scribe import scribe
            from thrift.transport import TTransport, TSocket
            from thrift.protocol import TBinaryProtocol

            log_entry = scribe.LogEntry(category, message)
            # depending on thrift version
            #   log_entry = scribe.LogEntry(dict(category=category, message=message))

            socket = TSocket.TSocket(host=self.host, port=self.port)
            transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocol(trans=transport, strictRead=False, strictWrite=False)
            client = scribe.Client(iprot=protocol, oprot=protocol)

            transport.open()
            result = client.Log(messages=[log_entry])
            transport.close()

            if result == 0:
                #with open(spoolfile, 'w') as fout:
                #    fout.writelines(data[1:])
                log.info('success ' + str(result))
            else:
                log.critical('Error result ' + str(result))



class ScrawlSpoolFileToScribeRetryMem():
    # single file with header text layout

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                fin.truncate(0)
                log.debug('TRUNCATE file ' + str(self.path))
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        #print str(type(mem_data))
        #print str(mem_data)

        retryList = []
        count = 0
        for item in mem_data:
            count += 1
            log.debug('item ' + str(count))

            byte1 = flag = first_line[0:1]
            byte2 = catsz1 = first_line[1:2]
            byte3 = catsz2 = first_line[2:3]
            byte4 = catsz3 = first_line[3:4]

            catsz = str(catsz1) + str(catsz2) + str(catsz3)
            catsz_size = int(catsz)
            #print 'catsz_size ' + str(catsz_size)

            category = first_line[4:catsz_size + 4]
            #print str('category: ' + category)

            message = first_line[catsz_size + 4:-1]
            #print str('message: ' + message)

            #with open(self.path, 'w') as fout:
            #    fout.writelines(mem_data[1:])

            from scribe import scribe
            from thrift.transport import TTransport, TSocket
            from thrift.protocol import TBinaryProtocol

            log_entry = scribe.LogEntry(category, message)
            # depending on thrift version
            #   log_entry = scribe.LogEntry(dict(category=category, message=message))

            socket = TSocket.TSocket(host=self.host, port=self.port)
            transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocol(trans=transport, strictRead=False, strictWrite=False)
            client = scribe.Client(iprot=protocol, oprot=protocol)

            transport.open()
            result = client.Log(messages=[log_entry])
            transport.close()

            if result == 0:
                #with open(spoolfile, 'w') as fout:
                #    fout.writelines(data[1:])
                log.info('success ' + str(result))
            else:
                log.critical('Error result ' + str(result))
                retryDict[count] = message

        while retryDict:
            try:
                for key, item in retryDict.iteritems():
                    print 'retry ' + str(item)
                    client = ScrawlTCPClient(self.host, self.port)
                    try:
                        result = client.send(category, message)
                    except:
                        log.critical('log.critical ' + str(result))

                    if result != True:
                        log.debug('Not OK.  queued message ...')
                        time.sleep(1)
                    else:
                        log.debug('ClientEnd ' + str(result))
                        del retryDict[key]
            except RuntimeError as e:
                log.debug('RuntimeError ' + str(e))
                if 'dictionary changed size during iteration' in e:
                    break



   
class WatchFileTaskSyncToScrawlTCPRetryMem:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, host, port):
        scrawl_spool_dir_file = n
        scrawl_host = host
        scrawl_port = port
        log.debug('drain ScrawlSpoolDirFileToScrawlTCPRetryMem ' + str(scrawl_spool_dir_file) + ' ' + str(scrawl_host) + ' ' + str(scrawl_port))
        drain = ScrawlSpoolDirFileToScrawlTCPRetryMem(scrawl_spool_dir_file, scrawl_host, scrawl_port)
        send = drain.send()
        #try:
        #    send = sink.send()
        #except TTransportException as e:
        #    log.warning('TTransportException: ' + str(e))
        log.debug(send)

#WatchFileTaskSyncToScribe
#WatchFileTaskSyncToScribeAndScrawlTCPRetryMem

class WatchFileTaskSyncToScribeAndScrawlTCPRetryMem:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, scribe_host, scribe_port, scrawl_host, scrawl_port):
        scrawl_spool_dir_file = n
        scribe_host = scribe_host
        scribe_port = scribe_port
        scrawl_host = scrawl_host
        scrawl_port = scrawl_port
        drain = ScrawlSpoolDirFileToScribeAndScrawlTCPRetryMem(scrawl_spool_dir_file, scribe_host, scribe_port, scrawl_host, scrawl_port)
        send = drain.send()
        log.debug(send)


class WatchFileTaskSyncToScribeRetryMem:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, host, port):
        scrawl_spool_dir_file = n
        scribe_host = host
        scribe_port = port
        drain = ScrawlSpoolDirFileToScribeRetryMem(scrawl_spool_dir_file, scribe_host, scribe_port)
        send = drain.send()
        log.debug(send)

class WatchFileTaskSyncToScribeRetryMemTimeOut:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, host, port):
        scrawl_spool_dir_file = n
        scribe_host = host
        scribe_port = port
        drain = ScrawlSpoolDirFileToScribeRetryMemTimeOut(scrawl_spool_dir_file, scribe_host, scribe_port)
        send = drain.send()
        log.debug(send)

class WatchFileTaskSyncToScribeRetryMemTimeOutSaveBack:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, host, port):
        scrawl_spool_dir_file = n
        scribe_host = host
        scribe_port = port
        drain = ScrawlSpoolDirFileToScribeRetryMemTimeOutSaveBack(scrawl_spool_dir_file, scribe_host, scribe_port)
        send = drain.send()
        log.debug(send)

     #ScrawlSpoolDirFileToScrawlTCPRetryMemTimeOutSaveBack fix.me.1 typo Srawl
     #ScrawlSpoolDirFileToSrawlTCPRetryMemTimeOutSaveBack
     #WatchFileTaskSyncToScrawlTCPRetryMemTimeOutSaveBack

class WatchMemTaskSyncToScribeRetryMemTimeOutSaveBack:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, category, message, host, port):
        category = category
        message = message
        scribe_host = host
        scribe_port = port
        drain = ScrawlSpoolMemFileToScribeRetryMemTimeOutSaveBack(category, message, scribe_host, scribe_port)
        send = drain.send()
        log.debug(send)

class WatchMemTaskSyncToScrawlRetryMemTimeOutSaveBack:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, category, message, host, port):
        category = category
        message = message
        scrawl_host = host
        scrawl_port = port
        drain = ScrawlSpoolMemFileToScrawlTCPRetryMemTimeOutSaveBack(category, message, scrawl_host, scrawl_port)
        send = drain.send()
        log.debug(send)




class WatchFileTaskSyncToScrawlTCPRetryMemTimeOutSaveBack:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, host, port):
        scrawl_spool_dir_file = n
        scrawl_host = host
        scrawl_port = port
               #ScrawlSpoolDirFileToScrawlTCPRetryMemTimeOutSaveBack
        drain = ScrawlSpoolDirFileToScrawlTCPRetryMemTimeOutSaveBack(scrawl_spool_dir_file, scrawl_host, scrawl_port)
        send = drain.send()
        log.debug(send)



class WatchFileTaskSyncToScribe:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, host, port):
        scrawl_spool_dir_file = n
        scribe_host = host
        scribe_port = port
        drain = ScrawlSpoolDirFileToScribe(scrawl_spool_dir_file, scribe_host, scribe_port)
        send = drain.send()
        #try:
        #    send = sink.send()
        #except TTransportException as e:
        #    log.warning('TTransportException: ' + str(e))
        log.debug(send)
        
        #while self._running and n > 0:
        #    #print str('WATCHQUEUE ' + watchQueue.get())
        #    #sys.stdout.write("  \b%s" % watchQueue.get())
        #    sys.stdout.write("  \b%s" % n)
        #    sys.stdout.flush()
        #    n += 1
        #    time.sleep(1)

class SendToScribeLoop:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, dictionary, host, port, category, message):

        self.dictionary = dictionary
        self.host = host
        self.port = port
        self.category = category
        self.message = message

        #while self._running and dictionary:
        #    for k in list(dictionary.keys()):
        #        print 'key ' + str(k) + ' value ' + str(dictionary[k])
        #        client = ScribeClient(host, port)
        #        result = client.send(category, message)
        #        if result == 0:
        #            log.debug('ScribeClient return ' + str(result))
        #            del dictionary[k]
        #        else:
        #            result = False
        #            pass
        #    time.sleep(1)


        #n = 10
        result = None
        n = len(dictionary.keys())
        while self._running and n > 0:
            log.debug('T-minus scribe ', n)
            for k in list(dictionary.keys()):
                client = ScribeClient(host, port)
                try:
                    result = client.send(category, message)
                #except TTransportException as e:
                except:
                    #print 'TTransportException'
                    log.critical('scribe TTransportException ' + str(result))
                 
                if result == 0:
                    log.debug('ScribeClient return ' + str(result))
                    n -= 1

            time.sleep(1)

class SendToScrawlLoop:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, dictionary, host, port, category, message):

        self.dictionary = dictionary
        self.host = host
        self.port = port
        self.category = category
        self.message = message

        #n = 10
        result = None
        n = len(dictionary.keys())
        while self._running and n > 0:
            log.debug('T-minus scrawl ', n)
            for k in list(dictionary.keys()):
                client = ScrawlTCPClient(host, port)
                try:
                    result = client.send(category, message)
                #except TTransportException as e:
                except:
                    #print 'ScrawlTCPClient'
                    log.debug('ScrawlTCPClient')

                #if result == 'OK':
                if result == True:
                    log.debug('SrawlTCPClient return ' + str(result))
                    n -= 1
                else:
                    log.debug('Bad return ' + str(result))

            time.sleep(1)

class BackLogWatchScrawl:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, scrawl_host, scrawl_port):

        CHECK_INTERVAL = 10

        success = None
        result  = None
        while self._running and n > 0:
            #sys.stdout.write("  \b%s" % n)
            #sys.stdout.flush()
            
            #check if backlog exists...
            scrawl_backlog_dir = '/var/spool/scrawl/backlog'
            dirList = os.listdir(scrawl_backlog_dir)
            backLog = []
            for category in dirList:
                scrawl_spool_dir_file = scrawl_backlog_dir + '/' + category + '/' + category + '_00000'
                if os.path.isfile(scrawl_spool_dir_file):
                    if os.stat(scrawl_spool_dir_file).st_size == 0:
                        os.unlink(scrawl_spool_dir_file)
                        log.debug('removed empty file ' + str(scrawl_spool_dir_file))
                    else:
                        backLog.append(scrawl_spool_dir_file)

            if backLog:

                for backlogfile in backLog:
                    log.debug(backlogfile) 

                    category = 'scrawl'
                    message  = 'scrawl BackLogWatchScrawl'
                    #client = ScrawlClientTCP(scrawl_host, scrawl_port)
                    client = ScrawlTCPClient(scrawl_host, scrawl_port)
                    try:
                        result = client.send(category, message)
                    except:
                        #party.foul
                        #log.debug('scribe TTransportException' + str(result))
                        log.debug('scrawl TCP client error' + str(result))
                    
                    if result is True:
                        success = 'success'
                        # deliver dir/file...
                        if os.path.isfile(backlogfile):
                            log.debug('proto send scrawl ' + str(scrawl_host) + ' port ' + str(scrawl_port))
                            drain = ScrawlSpoolDirFileToScrawlRetryMemTimeOutSaveBack(backlogfile, scrawl_host, scrawl_port)
                            try:
                                send = drain.send()
                            except IOError as e:
                                log.debug('IOError ' + str(e))
            else:
                log.debug('backLog empty')

            log.debug('scrawl backlog run again in ' + str(CHECK_INTERVAL) + ' ...')
            #n += 1
            time.sleep(CHECK_INTERVAL)


class BackLogWatchScribe:

    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, scribe_host, scribe_port):

        CHECK_INTERVAL = 10

        success = None
        result  = None
        while self._running and n > 0:
            #sys.stdout.write("  \b%s" % n)
            #sys.stdout.flush()
            
            #check if backlog exists...
            scrawl_backlog_dir = '/var/spool/scrawl/backlog'
            dirList = os.listdir(scrawl_backlog_dir)
            backLog = []
            for category in dirList:
                scrawl_spool_dir_file = scrawl_backlog_dir + '/' + category + '/' + category + '_00000'
                if os.path.isfile(scrawl_spool_dir_file):
                    if os.stat(scrawl_spool_dir_file).st_size == 0:
                        os.unlink(scrawl_spool_dir_file)
                        log.debug('removed empty file ' + str(scrawl_spool_dir_file))
                    else:
                        backLog.append(scrawl_spool_dir_file)

            if backLog:

                for backlogfile in backLog:
                    log.debug(backlogfile) 

                    category = 'scrawl'
                    message  = 'scrawl BackLogWatchScribe'
                    client = ScribeClient(scribe_host, scribe_port)
                    try:
                        result = client.send(category, message)
                    except:
                        log.debug('scribe TTransportException' + str(result))
                    
                    if result == 0:
                        success = 'success'
                        # deliver dir/file...
                        if os.path.isfile(backlogfile):
                            log.debug('proto send scribe ' + str(scribe_host) + ' port ' + str(scribe_port))
                            drain = ScrawlSpoolDirFileToScribeRetryMemTimeOutSaveBack(backlogfile, scribe_host, scribe_port)
                            try:
                                send = drain.send()
                            except IOError as e:
                                log.debug('IOError ' + str(e))
            else:
                log.debug('backLog empty')

            log.debug('scribe backlog run again in ' + str(CHECK_INTERVAL) + ' ...')
            #n += 1
            time.sleep(CHECK_INTERVAL)

class CountDownTask:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n):
        while self._running and n > 0:
            print('T-minus ', n)
            n -= 1
            time.sleep(1)


class CountUpTask:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n):
        while self._running and n > 0:
            sys.stdout.write("  \b%s" % n)
            sys.stdout.flush()
            n += 1
            time.sleep(1)





